import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnhover]'
})
export class OnhoverDirective {

  constructor() { }

  @HostListener('mouseenter') mouseover(){

  }

  @HostListener('mouseleave') mouseleave(){

  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AceEditorModule } from 'ngx-ace-editor-wrapper';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JEditorComponent } from './editors/JEditor/JEditor.component';
import { SinglepunchComponent } from './editors/singlepunch/singlepunch.component';
import { EditableComponent } from './editable/editable.component';
import { EditModeDirective } from './editable/edit-mode.directive';
import { ViewModeDirective } from './editable/view-mode.directive';
import { EditableOnEnterDirective } from './editable/editable-on-enter.directive';
import { DataService } from './data.service';
import { single } from 'rxjs/operators';
import { EssayComponent } from './editors/essay/essay.component';
import { BaseeditorComponent } from './editors/baseeditor/baseeditor.component';
import { OnhoverDirective } from './directives/onhover.directive';
import { FocusableDirective } from './directives/focusable.directive';
import { NumericComponent } from './editors/numeric/numeric.component';

@NgModule({
  declarations: [
    AppComponent,
    JEditorComponent,
    SinglepunchComponent,
    EditableComponent,
    EditModeDirective,
    ViewModeDirective,
    EditableOnEnterDirective,
    EssayComponent,
    BaseeditorComponent,
    OnhoverDirective,
    FocusableDirective,
    NumericComponent
  ],
  entryComponents: [
    EssayComponent,
    SinglepunchComponent,
    NumericComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AceEditorModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent, JEditorComponent]
})
export class AppModule { }

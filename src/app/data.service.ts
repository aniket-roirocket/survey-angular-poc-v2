import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { jsonData } from './data.items';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  message: string = JSON.stringify(jsonData, null, '\t');

  private editorUpdate = new BehaviorSubject(this.message);
  editorUpdateMessage = this.editorUpdate.asObservable();

  private htmlUpdate = new BehaviorSubject(this.message);
  htmlUpdateMessage = this.htmlUpdate.asObservable();

  constructor() { }

  updateEditorMessage(message: string) {
    this.editorUpdate.next(message)
  }

  updateHtmlMessage(message: string) {
    this.htmlUpdate.next(message)
  }

}

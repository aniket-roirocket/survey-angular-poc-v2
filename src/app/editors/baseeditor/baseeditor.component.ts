import { Component, OnInit } from '@angular/core';
import { jsonData } from '../../data.items';
import { FormGroup, FormArray, FormControl, Validators, NgModel } from '@angular/forms';
import { DataService } from "../../data.service";

@Component({
  selector: 'app-baseeditor',
  templateUrl: './baseeditor.component.html',
  styleUrls: ['./baseeditor.component.css']
})
export class BaseeditorComponent implements OnInit {

  model = jsonData;
  questionCtl = new FormControl();
  instrCtl = new FormControl();
  etype = this.model.question.type;

  constructor(protected dataService: DataService) { }

  ngOnInit(): void {

    this.dataService.htmlUpdateMessage.subscribe(msg =>
      (
        this.model = JSON.parse(msg),
        this.updateControlData()
      )
    );

  }

  updateControlData() {

    this.questionCtl = new FormControl(this.model.question.text);
    this.instrCtl = new FormControl(this.model.question.instructions);

  }

  getQuestionControl(): FormControl {
    return this.questionCtl as FormControl;
  }

  getInstrControl(): FormControl {
    return this.instrCtl as FormControl;
  }

  updateQuestion() {
    var control = this.getQuestionControl();

    if (control.valid) {
      this.model.question.text = control.value;
    }

    this.dataService.updateEditorMessage(JSON.stringify(this.model, null, '\t'));

    return control;
  }

  updateInstr() {
    var control = this.getInstrControl();

    if (control.valid) {
      this.model.question.instructions = control.value;
    }
    this.dataService.updateEditorMessage(JSON.stringify(this.model, null, '\t'));

    return control;
  }

}

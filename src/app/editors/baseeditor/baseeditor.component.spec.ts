import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BaseeditorComponent } from './baseeditor.component';

describe('BaseeditorComponent', () => {
  let component: BaseeditorComponent;
  let fixture: ComponentFixture<BaseeditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseeditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseeditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

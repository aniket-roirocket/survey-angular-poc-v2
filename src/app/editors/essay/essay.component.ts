import { Component, OnInit } from '@angular/core';
import { BaseeditorComponent } from '../baseeditor/baseeditor.component';
import { DataService } from "../../data.service";
import { ComponentLookup } from '../../data.items';

@ComponentLookup('essay')

@Component({
  selector: 'app-essay',
  templateUrl: './essay.component.html',
  styleUrls: ['./essay.component.css']
})
export class EssayComponent extends BaseeditorComponent implements OnInit {

  constructor(protected dataService: DataService) {
    super(dataService);
  }

  ngOnInit(): void {
    super.ngOnInit()
  }


}

import { AfterViewInit, Component, ComponentFactoryResolver, ViewChild, ViewContainerRef } from '@angular/core';
import { Ace } from 'ace-builds';
import { DataService } from "../../data.service";
import { ComponentLookupRegistry } from '../../data.items';

import 'brace';
//import { Editor } from 'brace';
import 'brace/mode/json';
import 'brace/theme/github';
import 'brace/theme/monokai';
import 'brace/theme/twilight';
import { BaseeditorComponent } from '../baseeditor/baseeditor.component';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';

import { QuestionTypes } from '../../data.items';

declare let ace: any;


@Component({
  selector: "app-JEditor",
  templateUrl: "./JEditor.component.html",
  styleUrls: ["./JEditor.component.scss"]
})

export class JEditorComponent implements AfterViewInit {
  @ViewChild('InputDynamicInsert', { read: ViewContainerRef })
  InputDynamicInsert!: ViewContainerRef;
  editor: any;
  jsonData!: string;
  options: any = { printMargin: false };

  questionTypes = QuestionTypes;
  selectedQType!: string;

  form!: FormGroup;
  qTypes: any = [];


  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private componentFactoryResolver: ComponentFactoryResolver) {

    this.form = this.formBuilder.group({
      qTypes: ['']
    });

    this.qTypes = this.questionTypes;

    this.selectedQType = this.qTypes[0].nk

    this.form.controls.qTypes.patchValue(this.selectedQType);

  }

  ngOnInit(): void {

    this.dataService.editorUpdateMessage.subscribe(msg => this.jsonData = msg);

  }

  ngAfterViewInit() {
    //const Range = ace.require('ace/range')['Range'];
    //this.editor.setTheme("eclipse");
    this.editor = ace.edit('json-editor');
    this.editor.setTheme('ace/theme/monokai');
    this.editor.resize()

    this.editor.getSession().setMode('ace/mode/json');

  }

  changeQType(e:any) {

    var type = this.form.get('qTypes');
    if (type != null) {
      var model = JSON.parse(this.jsonData);
      model.question.type = type.value;
      this.jsonData = JSON.stringify(model, null, '\t')
    }
  }

  onSaveJson() {

    var model = JSON.parse(this.editor.getValue());
    this.selectedQType = model.question.type.trim();
    this.form.controls.qTypes.patchValue(this.selectedQType);
    this.onSubmit();

  }
  onSubmit() {

    var model = JSON.parse(this.editor.getValue());
    var questionType = model.question.type.trim().toLocaleLowerCase();

    const classRef = ComponentLookupRegistry.get(questionType);

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(classRef);
    this.InputDynamicInsert.clear();
    const dyynamicComponent = <BaseeditorComponent>this.InputDynamicInsert.createComponent(componentFactory).instance;
    //use the service to send data
    //this.renderer2.addClass(compRef.location.nativeElement, 'flex');

    this.dataService.updateHtmlMessage(this.editor.getValue());
    //dyynamicComponent.model = model;

  }

}

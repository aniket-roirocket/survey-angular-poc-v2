import { NO_ERRORS_SCHEMA } from "@angular/core";
import { JEditorComponent } from "./JEditor.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";

describe("JEditorComponent", () => {

  let fixture: ComponentFixture<JEditorComponent>;
  let component: JEditorComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
      ],
      declarations: [JEditorComponent]
    });

    fixture = TestBed.createComponent(JEditorComponent);
    component = fixture.componentInstance;

  });

  it("should be able to create component instance", () => {
    expect(component).toBeDefined();
  });
  
});

import { Component, OnInit } from '@angular/core';
import { BaseeditorComponent } from '../baseeditor/baseeditor.component';
import { DataService } from "../../data.service";
import { ComponentLookup } from '../../data.items';

@Component({
  selector: 'app-numeric',
  templateUrl: './numeric.component.html',
  styleUrls: ['./numeric.component.css']
})

@ComponentLookup('numeric')

export class NumericComponent extends BaseeditorComponent implements OnInit {

  constructor(protected dataService: DataService) {
    super(dataService);
  }

  ngOnInit(): void {
    super.ngOnInit()
  }

}

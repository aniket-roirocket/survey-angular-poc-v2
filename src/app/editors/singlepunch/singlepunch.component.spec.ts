import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglepunchComponent } from './singlepunch.component';

describe('SinglepunchComponent', () => {
  let component: SinglepunchComponent;
  let fixture: ComponentFixture<SinglepunchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinglepunchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglepunchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

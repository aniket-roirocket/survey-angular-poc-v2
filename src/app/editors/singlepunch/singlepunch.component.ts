import { Component, ViewChild, AfterViewInit, Input, Output, EventEmitter, ViewEncapsulation, OnInit, ViewChildren, ViewContainerRef, QueryList, AfterViewChecked, AfterContentChecked } from '@angular/core';
import { jsonData, ComponentLookup } from '../../data.items';
import { FormGroup, FormArray, FormControl, Validators, NgModel } from '@angular/forms';
import { DataService } from "../../data.service";
import { BaseeditorComponent } from '../baseeditor/baseeditor.component';
import { EditableComponent } from 'src/app/editable/editable.component';

@ComponentLookup('singlePunch,multiPunch')
@Component({
  selector: 'app-singlepunch',
  templateUrl: './singlepunch.component.html',
  styleUrls: ['./singlepunch.component.css']
})

export class SinglepunchComponent extends BaseeditorComponent implements AfterViewInit, OnInit {
  @ViewChildren(EditableComponent) private editOptionRows!: QueryList<EditableComponent>;

  optionCtl: FormArray = new FormArray([]);
  controlType: string = "radio";

  constructor(protected dataService: DataService) {
    super(dataService);
  }

  ngAfterViewInit(): void{

    this.editOptionRows.changes.subscribe(() => {
      //this.editOptionRows.toArray()[3].toEditMode();
    })

  }

  ngOnInit(): void {

    super.ngOnInit()
    this.controlType = this.model.question.type == "singlePunch" ? "radio" : "checkbox";

  }

  updateControlData() {

    this.questionCtl = new FormControl(this.model.question.text);
    this.instrCtl = new FormControl(this.model.question.instructions);

    if (this.model.question.rows != null && this.model.question.rows.length > 0) {
      const toGroups = this.model.question.rows.map(entity => {
        return new FormGroup({
          name: new FormControl(entity.name, Validators.required),
          text: new FormControl(entity.text),
          value: new FormControl(entity.value)
        });
      });

      this.optionCtl = new FormArray(toGroups);

    }
  }

  getControlByName(ctlName: string, field: string): FormControl {
    var control = this.optionCtl.value.filter((p: { name: string | string[]; }) => p.name.includes(ctlName))
    return control.get(field) as FormControl;
  }

  getControl(index: number, field: string): FormControl {

    return this.optionCtl.at(index).get(field) as FormControl;
  }

  enterClick(index: number) {

    this.addOption(index);

    this.dataService.updateEditorMessage(JSON.stringify(this.model, null, '\t'));
    //this.editOptionRows.toArray()[5].toEditMode();

    //alert(this.editOptionRows.toArray().length)
    //this.editOptionRows.toArray()[index+3].toEditMode();

  }

  updateField(index: number, field: string) {

    const control = this.getControl(index, field);

    if (control.valid) {
      this.model.question.rows = this.model.question.rows.map((e, i) => {
        if (index === i) {
          return {
            ...e,
            [field]: control.value
          }
        }
        return e;
      })

      this.dataService.updateEditorMessage(JSON.stringify(this.model, null, '\t'));
    }

  }

  deleteOption(index: number) {
    this.optionCtl.removeAt(index);
    this.model.question.rows.splice(index, 1);
    this.dataService.updateEditorMessage(JSON.stringify(this.model, null, '\t'));
  }

  addOption(index: number) {
    var newItem = {
      "text": "Click to write",
      "name": "R101",
      "value": 101
    }

    if (this.model.question.rows == null) {
      this.model.question.rows = new Array(0);
    }

    var grp = new FormGroup({
      name: new FormControl(newItem.name, Validators.required),
      text: new FormControl(newItem.text),
      value: new FormControl(newItem.value)
    });

    if (index == -1) {
      this.model.question.rows.push(newItem);
      this.optionCtl.push(grp);
    }
    else {
      this.model.question.rows.splice(index + 1, 0, newItem);
      this.optionCtl.insert(index + 1, grp);
    }

  }

}

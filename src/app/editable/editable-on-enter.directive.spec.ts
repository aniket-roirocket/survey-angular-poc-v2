import { EditableOnEnterDirective } from './editable-on-enter.directive';
import { EditableComponent } from './editable.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';

describe('EditableOnEnterDirective', () => {
  let component: EditableComponent;
  let fixture: ComponentFixture<EditableComponent>;
  fixture = TestBed.createComponent(EditableComponent);
  component = fixture.componentInstance;

  it('should create an instance', () => {
    const directive = new EditableOnEnterDirective(component);
    expect(directive).toBeTruthy();
  });
});

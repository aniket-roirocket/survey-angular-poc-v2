export const jsonData =
{
  "question": {
    "name": "Q1a",
    "text": "Based on the name alone would watch this movies?",
    "instructions": "(Click one response)",
    "type": "singlePunch",
    "rows": [
      {
        "text": "Definitely would",
        "name": "R5",
        "value": 5
      },
      {
        "text": "Probably would",
        "name": "R4",
        "value": 4
      },
      {
        "text": "Might or might not",
        "name": "R3",
        "value": 3
      },
      {
        "text": "Probably would <u>not</u>",
        "name": "R2",
        "value": 2
      },
      {
        "text": "Definitely would <u>not</u>",
        "name": "R1",
        "value": 1
      }
    ]
  }
}

export const QuestionTypes =
  [
    { id: 1, name: "Single Punch", nk: "singlePunch", selected: true },
    { id: 2, name: "Multi Punch", nk: "multiPunch" },
    { id: 3, name: "Essay", nk: "essay" },
    { id: 4, name: "Numeric", nk: "numeric" }
  ];

export const ComponentLookupRegistry: Map<string, any> = new Map();

export const ComponentLookup = (key: string): any => {

  var keys = key.split(',');

  return (myComp: any) => {
    keys.forEach(function (value) {
      ComponentLookupRegistry.set(value.trim().toLocaleLowerCase(), myComp);
    })
  };

};
